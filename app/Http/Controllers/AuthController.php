<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function __construct() {
        $this->api_url = env('API_DEFAULT');
    }

    public function login() {
        if ($this->auth() == 1) {
            return redirect()->route('home');
        } else {
            return view('login');
        }
    }

    public function login_post(Request $request) {
        $data = [
            "username"  => $request->username,
            "password"  => $request->password
        ];

        $http_post = $this->http_post($this->api_url.'/login', $data);

        if ($http_post['status'] == 200) {
            // Create Session Super Admin
            session_start();
            $_SESSION['SUPERADMIN']['IS_LOGIN']     = true;
            $_SESSION['SUPERADMIN']['ID']           = $http_post['data']['ID'];
            $_SESSION['SUPERADMIN']['USERNAME']     = $http_post['data']['USERNAME'];
            $_SESSION['SUPERADMIN']['EMAIL']        = $http_post['data']['EMAIL'];
            $_SESSION['SUPERADMIN']['FULLNAME']     = $http_post['data']['FULLNAME'];
            $_SESSION['SUPERADMIN']['ROLE']         = $http_post['data']['ROLE']; 
            $_SESSION['SUPERADMIN']['TOKEN']        = $http_post['data']['TOKEN'];

            return redirect()->route('home');
        } else {
            return redirect()->back()->with(['message' => $http_post['message']]);
        }
    }

    public function logout() {
        if ($this->auth() == 1) {
            if (isset($_SESSION['SUPERADMIN']) && $_SESSION['SUPERADMIN']['IS_LOGIN'] == true) {
                $http_get = $this->http_get($this->api_url.'/logout'.'/'.$_SESSION['SUPERADMIN']['ID']);
                
                if (isset($http_get) && $http_get['status'] == 200) {
                    ## Hapus Session
                    session_destroy();

                    return redirect()->route('login')->with(['message' => $http_get['message']]);
                } else {
                    return redirect()->back();
                }
            } else {
                return redirect()->back();
            }
        } else {
            return $this->login_failed();
        }
    }

    public function logs() {
        if ($this->auth() == 1) {
            $http_get = $this->http_get($this->api_url.'users-logs/'.$_SESSION['SUPERADMIN']['ID']);

            if ($http_get['status'] == 200) {
                return response()->json($http_get->json());
            } else {
                return response()->json([]);
            }
        } else {
            return $this->login_failed();
        }
    }
}
