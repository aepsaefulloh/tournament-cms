<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function __construct() {
        $this->api_url = env('API_URL');
    }

    public function index() {
        if ($this->auth() == 1) {
            return view('payment.transaction.index');
        } else {
            return $this->login_failed();
        }
    }

    public function datatables() {
        if ($this->auth() == 1) {
            $http_get = $this->http_get($this->api_url.'payment-transaction?type=table');

            if ($http_get['status'] == 200) {
                return response()->json($http_get->json());
            } else {
                return response()->json([]);
            }
        } else {
            return $this->login_failed();
        }
    }

    public function edit($id) {
        if ($this->auth() == 1) {
            // Get Data Transaction
            $http_get_transaction = $this->http_get($this->api_url.'payment-transaction/'.$id);

           if (isset($http_get_transaction) && $http_get_transaction['status'] == 200) {
                return view('payment.transaction.form', [
                    'id'            => $id,
                    'transaction'   => (isset($http_get_transaction) && $http_get_transaction['status'] == 200) ? $http_get_transaction['data'] : []
                ]);
           } else {
               return abort(404);
           }
        } else {
            return $this->login_failed();
        }
    }

    public function store(Request $request) {
        if ($this->auth() == 1) {
             // Filter Rp Price to integer
             if ($request->shipping != '') {
                $shipping = intVal(preg_replace('/\D/', "", $request->shipping));
            } else {
                $shipping = '';
            }

            $http_post = $this->http_post($this->api_url.'payment-transaction-store', [
                'id'        => $request->id,
                'fullname'  => $request->fullname,
                'address'   => $request->address,
                'phone'     => $request->phone,
                'email'     => $request->email,
                'kodepos'   => $request->kodepos,
                'courier'   => $request->courier,    
                'shipping'  => $shipping,
                'status'    => $request->status
            ]);

            if (isset($http_post) && $http_post['status'] == 200) {
                return redirect()->back()->with(['message' => $http_post['message']]);
            } else {
                return redirect()->back()->with(['message' => $http_post['message']]);
            }
        } else {
            return $this->login_failed();
        }
    }
}
