<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index() {
        if ($this->auth() == 1) {
            return view('home');
        } else {
            return $this->login_failed();
        }
    }
}
