<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    public function __construct() {
        $this->api_url = env('API_URL');
    }

    public function index() {
        if ($this->auth() == 1) {
            return view('content.banner.index');
        } else {
            return $this->login_failed();
        }
    }

    public function datatables() {
        if ($this->auth() == 1) {
            $http_get = $this->http_get($this->api_url.'content-banner?type=table');

            if ($http_get['status'] == 200) {
                return response()->json($http_get->json());
            } else {
                return response()->json([]);
            }
        } else {
            return $this->login_failed();
        }
    }

    public function add() {
        if ($this->auth() == 1) {
            return view('content.banner.form', ['mode' => 'Tambah']);
        } else {
            return $this->login_failed();
        }
    }

    public function edit($id) {
        if ($this->auth() == 1) {
            // Get Data Banner
            $http_get_banner = $this->http_get($this->api_url.'content-banner/'.$id);

           if (isset($http_get_banner) && $http_get_banner['status'] == 200) {
                return view('content.banner.form', [
                    'mode'   => "Ubah",
                    'id'     => $id,
                    'banner' => (isset($http_get_banner) && $http_get_banner['status'] == 200) ? $http_get_banner['data'] : []
                ]);
           } else {
               return abort(404);
           }
        } else {
            return $this->login_failed();
        }
    }

    public function store(Request $request) {
        if ($this->auth() == 1) {
            // Convert Image to Base64 Code
            if ($request->file('gambar')) {
                $type = $request->file('gambar')->getClientOriginalExtension();
                $convert64 = 'data:image/' . $type . ';base64,' .base64_encode(file_get_contents($request->file('gambar'))); 
            } else {
                $convert64 = '';
                $type = '';
            }
            
            $http_post = $this->http_post($this->api_url.'content-banner-store', [
                'id'        => $request->id,
                'title'     => $request->title,
                'type'      => null,
                'position'  => $request->position,
                'url'       => $request->url,
                'ordernum'  => $request->ordernum,
                'status'    => $request->status,
                'image'     => [
                    'name'  => $convert64,
                    'type'  => $type
                ]
            ]);

            if (isset($http_post) && $http_post['status'] == 200) {
                if ($request->mode == 'Ubah') {
                    return redirect()->back()->with(['message' => $http_post['message']]);
                } else {
                    return redirect()->route('content-banner.index')->with(['message' => $http_post['message']]);
                }
            } else {
                return redirect()->back()->with(['message' => $http_post['message']]);
            }
        } else {
            return $this->login_failed();
        }
    }
}
