<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StockController extends Controller
{
    public function __construct() {
        $this->api_url = env('API_URL');
    }

    public function index() {
        if ($this->auth() == 1) {
            return view('product.stock.index');
        } else {
            return $this->login_failed();
        }
    }

    public function datatables() {
        if ($this->auth() == 1) {
            $http_get = $this->http_get($this->api_url.'stock?type=table');

            if ($http_get['status'] == 200) {
                return response()->json($http_get->json());
            } else {
                return response()->json([]);
            }
        } else {
            return $this->login_failed();
        }
    }

    public function stock_balance() {
        if ($this->auth() == 1) {
            $http_get = $this->http_get($this->api_url.'stock-balance');

            if ($http_get['status'] == 200) {
                return response()->json($http_get['data']);
            } else {
                return response()->json([]);
            }
        } else {
            return $this->login_failed();
        }
    }

    public function add() {
        if ($this->auth() == 1) {
            // Get Data Product
            $http_get_product = $this->http_get($this->api_url.'product-list');

            if (isset($http_get_product) && $http_get_product['status'] == 200 && count($http_get_product['data']) > 0) {
                $product = $http_get_product['data'];
            } else {
                $product = [];
            }
            
            return view('product.stock.form', ['mode' => 'Tambah', 'product' => $product]);
        } else {
            return $this->login_failed();
        }
    }

    public function edit($id) {
        if ($this->auth() == 1) {
            // Get Data Stock
            $http_get_stock = $this->http_get($this->api_url.'stock/'.$id);
            
            // Get Product
            $http_get_product = $this->http_get($this->api_url.'product-list');

            if (isset($http_get_product) && $http_get_product['status'] == 200 && count($http_get_product['data']) > 0) {
                $product = $http_get_product['data'];
            } else {
                $product = [];
            }
            
           if (isset($http_get_stock) && $http_get_stock['status'] == 200) {
                return view('product.stock.form', [
                    'mode'      => "Ubah",
                    'id'        => $id,
                    'product'  => $product,
                    'stock'     => (isset($http_get_stock) && $http_get_stock['status'] == 200) ? $http_get_stock['data'] : []
                ]);
           } else {
               return abort(404);
           }
        } else {
            return $this->login_failed();
        }
    }

    public function store(Request $request) {
        if ($this->auth() == 1) {
            $http_post = $this->http_post($this->api_url.'stock-store', [
                'id'                => $request->id,
                'date'              => $request->date,
                'product'           => $request->product,
                'size'              => $request->size,
                'width'             => $request->width,
                'length'            => $request->length,
                'total'             => $request->total,
                'status'            => $request->status,
            ]);

            if (isset($http_post) && $http_post['status'] == 200) {
                if ($request->mode == 'Ubah') {
                    return redirect()->back()->with(['message' => $http_post['message']]);
                } else {
                    return redirect()->route('stock.index')->with(['message' => $http_post['message']]);
                }
            } else {
                return redirect()->back()->with(['message' => $http_post['message']]);
            }
        } else {
            return $this->login_failed();
        }
    }
}
