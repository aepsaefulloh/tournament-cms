<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct() {
        $this->api_url = env('API_URL');
    }

    public function index() {
        if ($this->auth() == 1) {
            return view('product.index');
        } else {
            return $this->login_failed();
        }
    }

    public function datatables() {
        if ($this->auth() == 1) {
            $http_get = $this->http_get($this->api_url.'product?type=table');

            if ($http_get['status'] == 200) {
                return response()->json($http_get->json());
            } else {
                return response()->json([]);
            }
        } else {
            return $this->login_failed();
        }
    }

    public function add() {
        if ($this->auth() == 1) {
            // Get Data Category Product
            $http_get_category = $this->http_get($this->api_url.'category-list?type=product&status=1');

            if (isset($http_get_category) && $http_get_category['status'] == 200 && count($http_get_category['data']) > 0) {
                $category = $http_get_category['data'];
            } else {
                $category = [];
            }

            return view('product.form', ['mode' => 'Tambah', 'category' => $category]);
        } else {
            return $this->login_failed();
        }
    }

    public function edit($id) {
        if ($this->auth() == 1) {
            // Get Data Product
            $http_get_product = $this->http_get($this->api_url.'product/'.$id);

            // Get Data Category
            $http_get_category = $this->http_get($this->api_url.'category-list?type=product&status=1');

            if (isset($http_get_category) && $http_get_category['status'] == 200) {
                $category = $http_get_category['data'];
            } else {
                $category = [];
            }

           if (isset($http_get_product) && $http_get_product['status'] == 200) {
                return view('product.form', [
                    'mode'      => "Ubah",
                    'id'        => $id,
                    'category'  => $category,
                    'product'   => (isset($http_get_product) && $http_get_product['status'] == 200) ? $http_get_product['data'] : []
                ]);
           } else {
               return abort(404);
           }
        } else {
            return $this->login_failed();
        }
    }

    public function store(Request $request) {
        if ($this->auth() == 1) {
            //  Convert Image to Base64 Code
            if ($request->image_default) {
                $convert64_def = 'data:image/' . $request->file('image_default')->getClientOriginalExtension() . ';base64,' .base64_encode(file_get_contents($request->file('image_default')));
            } else {
                $convert64_def = '';
            }
            if ($request->images && count($request->images) > 0) {
                foreach ($request->images as $key => $image) {
                    $convert64[$key] = 'data:image/' . $image->getClientOriginalExtension() . ';base64,' .base64_encode(file_get_contents($image));
                }
            } else {
                $convert64 = '';
            
            }

            // Filter Rp Price to integer
            if ($request->price != '') {
                $price = intVal(preg_replace('/\D/', "", $request->price));
            } else {
                $price = '';
            }

            $http_post = $this->http_post($this->api_url.'product-store', [
                'id'            => $request->id,
                'product'       => $request->product,
                'code'          => $request->code,
                'category'      => $request->category,
                'price'         => $price,
                'summary'       => $request->summary,
                'specs'         => $request->specs,
                'date'          => $request->date,
                'status'        => $request->status,
                'avail'         => $request->avail,
                'image_def'     => "$convert64_def",
                'image'         => $convert64
            ]);


            if (isset($http_post) && $http_post['status'] == 200) {
                if ($request->mode == 'Ubah') {
                    return redirect()->back()->with(['message' => $http_post['message']]);
                } else {
                    return redirect()->route('product.index')->with(['message' => $http_post['message']]);
                }
            } else {
                return redirect()->back()->with(['message' => $http_post['message']]);
            }
        } else {
            return $this->login_failed();
        }
    }

    public function remove_image(Request $request) {
        if ($this->auth() == 1) {
            $http_get = $this->http_get($this->api_url.'product-remove-image?type='.$request->type.'&id='.$request->id);

            if (isset($http_get) && $http_get['status'] == 200) {
                return redirect()->back()->with(['message' => $http_get['message']]);
            } else {
                return redirect()->back()->with(['message' => $http_get['message']]);
            }
        } else {
            return $this->login_failed();
        }
    }

    public function status_product(Request $request) {
        if ($this->auth() == 1) {
            $http_post = $this->http_post($this->api_url.'product-status', [
                'id'            => $request->id,
                'status'        => $request->status
            ]);


            if (isset($http_post) && $http_post['status'] == 200) {
                return redirect()->back()->with(['message' => $http_post['message']]);
            } else {
                return redirect()->back()->with(['message' => $http_post['message']]);
            }
        } else {
            return $this->login_failed();
        }
    }
}
