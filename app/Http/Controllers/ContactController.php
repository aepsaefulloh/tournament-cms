<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function __construct() {
        $this->api_url = env('API_URL');
    }

    public function index() {
        if ($this->auth() == 1) {
            return view('contact.index');
        } else {
            return $this->login_failed();
        }
    }
    
    public function datatables() {
        if ($this->auth() == 1) {
            $http_get = $this->http_get($this->api_url.'contact?type=table');

            if ($http_get['status'] == 200) {
                return response()->json($http_get->json());
            } else {
                return response()->json([]);
            }
        } else {
            return $this->login_failed();
        }
    }
}