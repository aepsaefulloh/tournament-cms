<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function __construct() {
        $this->api_url = env('API_URL');
    }

    public function index() {
        if ($this->auth() == 1) {
            return view('account.index');
        } else {
            return $this->login_failed();
        }
    }

    public function datatables() {
        if ($this->auth() == 1) {
            $http_get = $this->http_get($this->api_url.'account-user?type=table');

            if ($http_get['status'] == 200) {
                return response()->json($http_get->json());
            } else {
                return response()->json([]);
            }
        } else {
            return $this->login_failed();
        }
    }

    public function add() {
        if ($this->auth() == 1) {
            $http_get_role = $this->http_get($this->api_url.'account-role-list?status=1');
            
           return view('account.users.form', [
               'mode'   => "Tambah",
               'roles'  => (isset($http_get_role) && $http_get_role['status'] == 200) ? $http_get_role['data'] : []
            ]);
        } else {
            return $this->login_failed();
        }
    }

    public function edit($id) {
        if ($this->auth() == 1) {
            // Get Data Account Role
            $http_get_role = $this->http_get($this->api_url.'account-role-list?status=1');
            
            // Get Data Account User
            $http_get_user = $this->http_get($this->api_url.'account/'.$id);

           if (isset($http_get_user) && $http_get_user['status'] == 200) {
                return view('account.users.form', [
                    'mode'   => "Ubah",
                    'id'     => $id,
                    'roles'  => (isset($http_get_role) && $http_get_role['status'] == 200) ? $http_get_role['data'] : [],
                    'user'   => (isset($http_get_user) && $http_get_user['status'] == 200) ? $http_get_user['data'] : []
                ]);
           } else {
               return abort(404);
           }
        } else {
            return $this->login_failed();
        }
    }

    public function store(Request $request) {
        if ($this->auth() == 1) {
            $http_post = $this->http_post($this->api_url.'account-store', [
                'id'            => $request->id, 
                'email'         => $request->email,
                'username'      => $request->username,
                'password_old'  => $request->password_old,
                'password'      => $request->password,
                'fullname'      => $request->fullname,
                'phone'         => $request->phone,
                'role'          => $request->role,
                'status'        => $request->status
            ]);

            if (isset($http_post) && $http_post['status'] == 200) {
                if ($request->mode == 'Ubah') {
                    return redirect()->back()->with(['message' => $http_post['message']]);
                } else {
                    return redirect()->route('account.index')->with(['message' => $http_post['message']]);
                }
            } else {
                return redirect()->back()->with(['message' => $http_post['message']]);
            }
        } else {
            return $this->login_failed();
        }
    }
}
