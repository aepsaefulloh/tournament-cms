<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Http;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // ------------------------------------------- Authentikasi ----------------------------------------------------- //
    public function auth() {
        session_start();
        if (isset($_SESSION['SUPERADMIN']) && $_SESSION['SUPERADMIN']['IS_LOGIN'] == true) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function API_KEY() {
        // return '$2a$10$vm9zPtIala1j4GjBTuSkDOdrCQ9/tlFruMQQ7iwsxVCZryU1Dv7fW';
        return env('TOKEN_APP', '$2a$12$8r8NRGqWuxZuMsMz9HarMOgWzgEZDQ7NViaw309mzOsCOfK90OWme');
    }

    public function auth_expired() {
        unset($_SESSION['SUPERADMIN']);
        return redirect()->route('login')->with(['message' => 'Ups, Sepertinya Ada yang login pada akun ini!']);
    }

    public function login_failed() {
        return redirect()->route('login')->with(['message' => 'Maaf, Harus Login dulu!']);
    }

    // ------------------------------------------------- HTTP CLIENT ---------------------------------------------- //
    public function http_get($url) {
        if (str_contains($url, '?')) {
            $url .= '&access_token='.$this->API_KEY();
        }else{
            $url .= '?access_token='.$this->API_KEY();
        }
        $http_get = Http::withHeaders([
            'user_agent'=> $_SERVER['HTTP_USER_AGENT'],
            'token'     => (isset($_SESSION['SUPERADMIN']) && $_SESSION['SUPERADMIN']['TOKEN']) ? $_SESSION['SUPERADMIN']['TOKEN'] : null
        ])->get($url);

        // dd($http_get->body());

        if ($http_get->status() == 200) {
            return $http_get;
        } elseif ($http_get->status() == 401) {
            return [
                "status"    => 401,
                "message"   => "Auth Server Error!"
            ];
        } else {
            return [
                "status"    => 500,
                "message"   => "API Server Error!"
            ];
        }   
        
    }

    public function http_post($url, $data) {
        if (str_contains($url, '?')) {
            $url .= '&access_token='.$this->API_KEY();
        }else{
            $url .= '?access_token='.$this->API_KEY();
        }
        $http_post = Http::withHeaders([
            'user_agent'=> $_SERVER['HTTP_USER_AGENT'],
            'token'     => (isset($_SESSION['SUPERADMIN']) && $_SESSION['SUPERADMIN']['TOKEN']) ? $_SESSION['SUPERADMIN']['TOKEN'] : null
        ])->post($url, $data);
        // dd($http_post->body());
        if ($http_post->status() == 200) {
            return $http_post;
        } elseif ($http_post->status() == 401) {
            return [
                "status"    => 500,
                "message"   => "API Server Error!"
            ];
        } else {
            return [
                "status"    => 500,
                "message"   => "API Server Error!"
            ];
        }
        
    }
}
