<?php

namespace App\Http\Controllers\Tournament;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TournamentController extends Controller
{
    public function __construct() {
        $this->api_url = env('API_URL');
    }

    public function index() {
        if ($this->auth() == 1) {
            return view('tournament.index');
        } else {
            return $this->login_failed();
        }
    }

    public function datatables() {
        if ($this->auth() == 1) {
            $http_get = $this->http_get($this->api_url.'tournament?type=table');
          
            if ($http_get['status'] == 200) {
                return response()->json($http_get->json());
            } else {
                return response()->json([]);
            }
        } else {
            return $this->login_failed();
        }
    }

    public function add() {
        if ($this->auth() == 1) {
            // Get Category Data
            $http_get_category = $this->http_get($this->api_url.'category-list?type=project&status=1');
            
            if (isset($http_get_category) && $http_get_category['status'] == 200) {
                $category = $http_get_category['data'];
            } else {
                $category = [];
            }

            return view('tournament.form', ['mode' => 'Tambah', 'category' => $category]);
        } else {
            return $this->login_failed();
        }
    }

    public function edit($id) {
        if ($this->auth() == 1) {
            // Get Data project
            $http_get_project = $this->http_get($this->api_url.'project/'.$id);
            
            // Get Data Category
            $http_get_category = $this->http_get($this->api_url.'category-list?type=project&status=1');
            
            if (isset($http_get_category) && $http_get_category['status'] == 200) {
                $category = $http_get_category['data'];
            } else {
                $category = [];
            }

           if (isset($http_get_project) && $http_get_project['status'] == 200) {
                return view('tournament.form', [
                    'mode'      => "Ubah",
                    'id'        => $id,
                    'category'  => $category,
                    'project'   => (isset($http_get_project) && $http_get_project['status'] == 200) ? $http_get_project['data'] : []
                ]);
           } else {
               return abort(404);
           }
        } else {
            return $this->login_failed();
        }
    }

    public function upload_image(Request $request) {
        if ($this->auth() == 1) {     
            if($request->hasFile('upload')) {
                $convert64 = 'data:image/' . $request->file('upload')->getClientOriginalExtension() . ';base64,' .base64_encode(file_get_contents($request->file('upload')));
                
                // Post Upload Image
                $http_post = $this->http_post($this->api_url.'project-store-image-project', [
                    'image'     => $convert64 
                ]);
                
                $CKEditorFuncNum = $request->get('CKEditorFuncNum');
                
                if (isset($http_post) && $http_post['status'] == 200) {
                    $url = $http_post['data']['IMAGE'];
                } else {
                    $url = null;
                }
                
                $msg = 'Image uploaded successfully'; 
                $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
                   
                @header('Content-type: text/html; charset=utf-8'); 
                echo $response;
            }
        } else {
            return $this->login_failed();
        }
    }

    public function store(Request $request) {
        if ($this->auth() == 1) {
            // Convert Image to Base64 Code
            if ($request->file('gambar')) {
                $type = $request->file('gambar')->getClientOriginalExtension();
                $convert64 = 'data:image/' . $type . ';base64,' .base64_encode(file_get_contents($request->file('gambar'))); 
            } else {
                $convert64 = '';
                $type = '';
            }
            
            $http_post = $this->http_post($this->api_url.'project-store', [
                'id'                => $request->id,
                'title'             => $request->title,
                'summary'           => $request->summary,
                'keyword'           => $request->keyword,
                'subtitle'          => $request->subtitle,
                'creator'           => $request->creator,
                'category'          => $request->category,
                'link_sportify'     => $request->link_sportify,
                'artist'            => $request->artist,
                'external_url'      => $request->external_url,
                'seo'               => $request->seo,
                'project'           => $request->content,
                'status'            => $request->status,
                'image'     => [
                    'name'  => $convert64,
                    'type'  => $type
                ]
            ]);

            if (isset($http_post) && $http_post['status'] == 200) {
                if ($request->mode == 'Ubah') {
                    return redirect()->back()->with(['message' => $http_post['message']]);
                } else {
                    return redirect()->route('project.index')->with(['message' => $http_post['message']]);
                }
            } else {
                return redirect()->back()->with(['message' => $http_post['message']]);
            }
        } else {
            return $this->login_failed();
        }
    }
}
