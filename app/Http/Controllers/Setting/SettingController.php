<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function __construct() {
        $this->api_url = env('API_URL');
    }

    public function index() {
        if ($this->auth() == 1) {
            // Get Data Config Apps
            $http_get_config = $this->http_get($this->api_url.'setting-apps');

            if (isset($http_get_config) && $http_get_config['status'] == 200 && count($http_get_config['data']) > 0) {
                $config = $http_get_config['data'];
            } else {
                $config = [];
            }

            return view('setting.form', ['config' => $config]);
        } else {
            return $this->login_failed();
        }
    }

    public function store(Request $request) {
        if ($this->auth() == 1) {
            // Convert Image to Base64 Code
            if ($request->file('DD_LOGO')) {
                $type = $request->file('DD_LOGO')[0]->getClientOriginalExtension();
                $convert64 = 'data:image/' . $type . ';base64,' .base64_encode(file_get_contents($request->file('DD_LOGO')[0])); 
            } else {
                $convert64 = '';
                $type = '';
            }

            $http_post = $this->http_post($this->api_url.'setting-apps-store', [
                'DD_SITENAME'        => $request->DD_SITENAME,
                'DD_DESCRIPTION'     => $request->DD_DESCRIPTION,
                'DD_KEYWORD'         => $request->DD_KEYWORD,
                'DD_ADDRESS'         => $request->DD_ADDRESS,
                'DD_PHONE'           => $request->DD_PHONE,
                'DD_EMAIL'           => $request->DD_EMAIL,
                'DD_FB'              => $request->DD_FB,
                'DD_TW'              => $request->DD_TW,
                'DD_IG'              => $request->DD_IG,
                'DD_YT'              => $request->DD_YT,
                'DD_FTEXT'           => $request->DD_FTEXT,
                'DD_ABOUTYT'         => $request->DD_ABOUTYT,
                'DD_PRIVACY'         => $request->DD_PRIVACY,
                'DD_TERM'            => $request->DD_TERM,
                'DD_LOGO'            => [
                    "NAME"  => $convert64,
                    "TYPE"  => $type,
                ]
            ]);

            if (isset($http_post) && $http_post['status'] == 200) {
                return redirect()->back()->with(['message' => $http_post['message']]);
            } else {
                return redirect()->back()->with(['message' => $http_post['message']]);
            }
        } else {
            return $this->login_failed();
        }
    }
}
