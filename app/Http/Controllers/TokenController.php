<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TokenController extends Controller
{
    public function __construct() {
        $this->api_url = env('API_URL');
    }

    public function index() {
        if ($this->auth() == 1) {
            return view('token-apps.index');
        } else {
            return $this->login_failed();
        }
    }

    public function datatables() {
        if ($this->auth() == 1) {
            $http_get = $this->http_get($this->api_url.'token-apps?type=table');

            if ($http_get['status'] == 200) {
                return response()->json($http_get->json());
            } else {
                return response()->json([]);
            }
        } else {
            return $this->login_failed();
        }
    }

    public function add() {
        if ($this->auth() == 1) {
            return view('token-apps.form', ['mode' => 'Tambah']);
        } else {
            return $this->login_failed();
        }
    }

    public function edit($id) {
        if ($this->auth() == 1) {
            // Get Data Token Apps
            $http_get_token = $this->http_get($this->api_url.'token-apps/'.$id);

           if (isset($http_get_token) && $http_get_token['status'] == 200) {
                return view('token-apps.form', [
                    'mode'      => "Ubah",
                    'id'        => $id,
                    'token'     => (isset($http_get_token) && $http_get_token['status'] == 200) ? $http_get_token['data'] : []
                ]);
           } else {
               return abort(404);
           }
        } else {
            return $this->login_failed();
        }
    }

    public function store(Request $request) {
        if ($this->auth() == 1) {
            $http_post = $this->http_post($this->api_url.'token-apps-store', [
                'id'            => $request->id,
                'name'          => $request->name,
                'token'         => $request->token,
                'status'        => $request->status,
                'date'          => \Carbon\Carbon::now()->format('Y-m-d G:i:s')
            ]);

            if (isset($http_post) && $http_post['status'] == 200) {
                if ($request->mode == 'Ubah') {
                    return redirect()->back()->with(['message' => $http_post['message']]);
                } else {
                    return redirect()->route('token-apps.index')->with(['message' => $http_post['message']]);
                }
            } else {
                return redirect()->back()->with(['message' => $http_post['message']]);
            }
        } else {
            return $this->login_failed();
        }
    }
}
