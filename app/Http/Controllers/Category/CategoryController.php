<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct() {
        $this->api_url = env('API_URL');
    }

    public function index() {
        if ($this->auth() == 1) {
            return view('category.index');
        } else {
            return $this->login_failed();
        }
    }

    public function datatables() {
        if ($this->auth() == 1) {
            $http_get = $this->http_get($this->api_url.'category?type=table');

            if ($http_get['status'] == 200) {
                return response()->json($http_get->json());
            } else {
                return response()->json([]);
            }
        } else {
            return $this->login_failed();
        }
    }

    public function add() {
        if ($this->auth() == 1) {
            return view('category.form', ['mode' => 'Tambah']);
        } else {
            return $this->login_failed();
        }
    }

    public function edit($id) {
        if ($this->auth() == 1) {
            // Get Data Category
            $http_get_category = $this->http_get($this->api_url.'category/'.$id);
           if (isset($http_get_category) && $http_get_category['status'] == 200) {
                return view('category.form', [
                    'mode'      => "Ubah",
                    'id'        => $id,
                    'category'  => (isset($http_get_category) && $http_get_category['status'] == 200) ? $http_get_category['data'] : []
                ]);
           } elseif (isset($http_get_category) && $http_get_category['status'] == 401) {
                return $this->auth_expired();
           } else {
               return abort(404);
           }
        } else {
            return $this->login_failed();
        }
    }

    public function store(Request $request) {
        if ($this->auth() == 1) {
            $http_post = $this->http_post($this->api_url.'category-store', [
                'id'            => $request->id,
                'category'      => $request->category,
                'seo'           => $request->seo,
                'tipe'          => $request->tipe,
                'status'        => $request->status,
                'level'         => $request->level,
                'parent_id'     => $request->parent_id
            ]);

            if (isset($http_post) && $http_post['status'] == 200) {
                if ($request->mode == 'Ubah') {
                    return redirect()->back()->with(['message' => $http_post['message']]);
                } else {
                    return redirect()->route('category.index')->with(['message' => $http_post['message']]);
                }
            } else {
                return redirect()->back()->with(['message' => $http_post['message']]);
            }
        } else {
            return $this->login_failed();
        }
    }
}
