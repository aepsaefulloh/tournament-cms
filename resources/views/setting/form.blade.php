@extends('layouts.v1.app')

@section('title', 'Form Setting Apps')

@section('setting', 'menu-item-active')

@section('content')
    {{-- Sub Header Content --}}
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Setting Apps</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">Form Setting Apps</span>
            </div>
            
            <div class="d-flex align-items-center">
                <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">
                    {{ \Carbon\Carbon::now()->format('d m Y') }} <span id="jam"></span> : <span id="menit"></span> : <span id="detik"></span>
                </a>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        {{-- Content --}}
        <div class="container">
            @if ($message = Session::get('message'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Form Setting Apps</h3>
                </div>
                <!--begin::Form-->
                <form class="form" action="{{ route('setting.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="card-body">
                        @if (count($config) > 0)
                            @foreach ($config as $item)
                                <div class="form-group">
                                    <label>{{ $item['LABEL'] }}</label>
                                    @if ($item['CTYPE'] == 'textarea')
                                        <input type="hidden" name="{{ $item['CKEY'] }}[]" value="{{ $item['ID'] }}">
                                        <textarea name="{{ $item['CKEY'] }}[]" class="form-control form-control-solid" id="{{ $item['CKEY'] }}" placeholder="Masukan {{ $item['LABEL'] }}">{{ $item['CVALUE'] }}</textarea>    
                                        <input type="hidden" name="{{ $item['CKEY'] }}[]" value="{{ $item['CKEY'] }}">
                                    @elseif ($item['CTYPE'] == 'image')
                                        <div class="text-center" id="box_img_preview">
                                            <img src="{{ $item['CVALUE'] }}" class="w-50 mb-2"><br>
                                            <button type="button" class="btn btn-sm btn-primary" id="btn_update_image">Perbaharui Gambar</button>
                                        </div>
        
                                        <div id="box_image" style="display: none;">
                                            <input type="file" name="{{ $item['CKEY'] }}[]" class="form-control form-control-solid" id="{{ $item['CKEY'] }}">
                                            <center>
                                                <img id="preview-image-before-upload" src="" alt="preview image" class="w-50 mt-5">
                                            </center>
                                        </div>
                                    @else 
                                        <input type="hidden" name="{{ $item['CKEY'] }}[]" value="{{ $item['ID'] }}">
                                        <input type="{{ $item['CTYPE'] }}" name="{{ $item['CKEY'] }}[]" class="form-control form-control-solid" id="{{ $item['CKEY'] }}" placeholder="Masukan {{ $item['LABEL'] }}" value="{{ $item['CVALUE'] }}">
                                        <input type="hidden" name="{{ $item['CKEY'] }}[]" value="{{ $item['CKEY'] }}">
                                        <span class="form-text text-muted">Just for Project Video Slider</span>
                                    @endif
                                </div>

                                @push('scripts')
                                <script src="{{ asset('metronic/plugins/ckeditor/ckeditor.js') }}"></script>
                                <script>
                                    $(document).ready(function (e) {
                                        // Onclick Update Image
                                        $('#btn_update_image').click(function() {
                                            $('#box_img_preview').hide();
                                            $('#box_image').show();
                                        })

                                        // Image Preview
                                        $('#preview-image-before-upload').hide();
                                        $("#{{ $item['CKEY'] }}").change(function(){
                                            let reader = new FileReader();
                                            reader.onload = (e) => { 
                                                $('#preview-image-before-upload').show().attr('src', e.target.result); 
                                            }
                                            reader.readAsDataURL(this.files[0]); 
                                        });
                                    });
                                </script>
                            @endpush
                            @endforeach
                        @else
                            
                        @endif
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <a class="btn btn-secondary mr-2" href="{{ route('content.index') }}">Kembali</a>
                            <button type="reset" class="btn btn-danger mr-2">Reset</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection