@extends('layouts.v1.app')

@section('title', 'Form Product')

@section('product', 'menu-item-active')

@section('content')
    {{-- Sub Header Content --}}
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Product</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">{{ $mode }} Product</span>
            </div>
            
            <div class="d-flex align-items-center">
                <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">
                    {{ \Carbon\Carbon::now()->format('d m Y') }} <span id="jam"></span> : <span id="menit"></span> : <span id="detik"></span>
                </a>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        {{-- Content --}}
        <div class="container-fluid">
            @if ($message = Session::get('message'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Form @if ($mode == "Tambah") Tambah @elseif ($mode == 'Ubah') Edit @endif Product</h3>
                </div>
                <!--begin::Form-->
                <form class="form" action="{{ route('product.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="mode" value="{{ $mode }}">
                    @if ($mode == 'Ubah') 
                        <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <div class="card-body">
                        <div class="form-group">
                            <label>Product</label>
                            <input type="text" name="product" class="form-control form-control-solid" id="product" placeholder="Masukan Nama Product*" @if ($mode == "Ubah" && isset($product)) value="{{ $product['PRODUCT'] }}" @endif>
                        </div>
                        
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Code</label>
                                    <input type="text" name="code" class="form-control form-control-solid" id="code" placeholder="Masukan Code Product" @if ($mode == "Ubah" && isset($product)) value="{{ $product['CODE'] }}" @endif>
                                </div>  
                            </div>
                            
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Category</label>
                                    <select name="category" class="form-control form-control-solid" id="category">
                                        <option value="">-- Pilih Category --</option>
                                        @foreach ($category as $item)
                                            <option value="{{ $item['ID'] }}" @if ($mode == "Ubah" && isset($product) && $product['CATEGORY'] == $item['ID']) selected @endif>{{ $item['CATEGORY'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Price</label>
                                    <input type="text" name="price" class="form-control form-control-solid" id="price" placeholder="Masukan Price Product" @if ($mode == "Ubah" && isset($product)) value="Rp. {{ number_format($product['PRICE'], 0, ',', '.') }}" @endif>
                                </div>
                            </div>
                            
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Summary</label>
                                    <input type="text" name="summary" class="form-control form-control-solid" id="summary" placeholder="Masukan Summary Product*" @if ($mode == "Ubah" && isset($product)) value="{{ $product['SUMMARY'] }}" @endif>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Gambar Utama</label>
                            <input type="file" name="image_default" class="form-control form-control-solid" id="image_default">    
                            <span class="form-text text-muted">Ukuran Gambar: 800x1000 & kurang dari 50kb</span>    
                            <center>
                                <img id="preview-image-before-upload" src="@if ($mode == 'Ubah' && isset($product) && $product['IMAGE']['DEFAULT'] != '') {{ $product['IMAGE']['DEFAULT'] }} @endif" alt="preview image" class="w-75 mt-5">
                            </center>
                        </div>

                        <div class="form-group">
                            <label>Gambar Lainnya</label>
                            @if ($mode == "Ubah")
                            <div class="row">
                                @foreach ($product['IMAGE']['ADD_IMAGE'] as $ai)
                                    <div class="col-4">
                                        <div class="text-center">
                                            <img src="{{ $ai['IMAGE'] }}" class="img-thumbnail"><br>
                                            <a href="{{ url('product-remove-image?type=add_image&id='.pathinfo($ai['IMAGE'], PATHINFO_BASENAME)) }}" class="badge badge-danger mt-2 mb-2">Remove</a>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="col-4">
                                    <div class="input-images"></div>
                                    <span class="form-text text-muted">Form Upload Gambar Lainnya</span>
                                    <span class="form-text text-muted">Ukuran Gambar: 800x1000 & kurang dari 50kb</span>
                                </div>
                            </div>
                            @else
                                <div class="input-images"></div>
                                <span class="form-text text-muted">Ukuran Gambar: 800x1000 & kurang dari 50kb</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Specification</label>
                            <textarea name="specs" class="form-control form-control-solid" id="specs">@if ($mode == "Ubah" && isset($product)) {{ $product['SPECS'] }} @else Masukan specification product @endif</textarea>
                        </div>

                        <div class="form-group">
                            <label>Tanggal</label>
                            <input type="datetime-local" name="date" class="form-control form-control-solid" id="date" @if ($mode == "Ubah" && isset($product)) value="{{ $product['CREATE_DATE'] }}" @endif>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Status Product</label>
                                    <select name="avail" class="form-control form-control-solid" id="avail">
                                        <option value="">-- Pilih Status Product --</option>
                                        <option value="0" @if ($mode == "Ubah" && isset($product) && $product['AVAIL'] == 0) selected @endif>Solt Out</option>
                                        <option value="1" @if ($mode == "Ubah" && isset($product) && $product['AVAIL'] == 1) selected @endif>Available</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Status</label>
                                    <select name="status" class="form-control form-control-solid" id="status">
                                        <option value="">-- Pilih Status --</option>
                                        <option value="0" @if ($mode == "Ubah" && isset($product) && $product['STATUS'] == 0) selected @endif>Unpublish</option>
                                        <option value="1" @if ($mode == "Ubah" && isset($product) && $product['STATUS'] == 1) selected @endif>Publish</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <a class="btn btn-secondary mr-2" href="{{ route('product.index') }}">Kembali</a>
                            <button type="reset" class="btn btn-danger mr-2">Reset</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    {{-- CKeditor --}}
    <script src="{{ asset('metronic/plugins/ckeditor/ckeditor.js') }}"></script>
    
    {{-- Material Icons Font --}}
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        
    {{-- Jquery Upload Multiple Image --}}
    <link type="text/css" rel="stylesheet" href="{{ asset('metronic/plugins/image-upload/image-uploader.min.css') }}">
    <script type="text/javascript" src="{{ asset('metronic/plugins/image-upload/image-uploader.min.js') }}"></script>
    
    {{-- JS Custom --}}
    <script>
        $(document).ready(function (e) {
            // Image Upload File Lainnya
            $('.input-images').imageUploader({
                label: 'Drag & Drop files here or click to browse',
                extensions: ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG', '.gif', '.GIF']
            });

            // Image Preview Default
            @if ($mode == 'Ubah' && isset($product) && $product['IMAGE']['DEFAULT'] != '')
                $('#preview-image-before-upload').show();
            @else 
                $('#preview-image-before-upload').hide();
            @endif
            $('#image_default').change(function(){
                let reader = new FileReader();
                reader.onload = (e) => { 
                    $('#preview-image-before-upload').show().attr('src', e.target.result); 
                }
                reader.readAsDataURL(this.files[0]); 
            });

            // Format RP Price
            var rupiah = document.getElementById('price');
            rupiah.addEventListener('keyup', function(e){
                rupiah.value = formatRupiah(this.value, 'Rp. ');
            });
    
            /* Fungsi formatRupiah */
            function formatRupiah(angka, prefix){
                var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split   		= number_string.split(','),
                sisa     		= split[0].length % 3,
                rupiah     		= split[0].substr(0, sisa),
                ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
    
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }
    
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
            }

            // CKEDITOR
            CKEDITOR.replace('specs');
        });
    </script>
@endpush