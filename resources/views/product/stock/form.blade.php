@extends('layouts.v1.app')

@section('title', 'Form Stock')

@section('stock', 'menu-item-active')

@section('content')
    {{-- Sub Header Content --}}
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Stock Product</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">{{ $mode }} Stock</span>
            </div>
            
            <div class="d-flex align-items-center">
                <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">
                    {{ \Carbon\Carbon::now()->format('d m Y') }} <span id="jam"></span> : <span id="menit"></span> : <span id="detik"></span>
                </a>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        {{-- Content --}}
        <div class="container">
            @if ($message = Session::get('message'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Form @if ($mode == "Tambah") Tambah @elseif ($mode == 'Ubah') Edit @endif Stock</h3>
                </div>
                <!--begin::Form-->
                <form class="form" action="{{ route('stock.store') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="mode" value="{{ $mode }}">
                    @if ($mode == 'Ubah') 
                        <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <div class="card-body">
                        <div class="form-group">
                            <label>Date</label>
                            <input type="date" name="date" class="form-control form-control-solid" id="date" @if ($mode == "Ubah" && isset($stock)) value="{{ $stock['STOCK_DATE'] }}" @endif>
                        </div>

                        <div class="form-group">
                            <label>Product</label>
                            <select name="product" class="form-control form-control-solid" id="product">
                                <option value="">-- Pilih Product --</option>
                                @foreach ($product as $p)
                                    <option value="{{ $p['CODE'] }}" @if ($mode == 'Ubah' && isset($stock) && $stock['CODE'] == $p['CODE']) selected @endif>{{ $p['PRODUCT'] }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Size</label>
                            <select name="size" class="form-control form-control-solid" id="size">
                                <option value="">-- Pilih Size --</option>
                                <option value="1" @if ($mode == 'Ubah' && isset($stock) && $stock['SIZE'] == 1) selected @endif>S</option>
                                <option value="2" @if ($mode == 'Ubah' && isset($stock) && $stock['SIZE'] == 2) selected @endif>M</option>
                                <option value="3" @if ($mode == 'Ubah' && isset($stock) && $stock['SIZE'] == 3) selected @endif>L</option>
                                <option value="4" @if ($mode == 'Ubah' && isset($stock) && $stock['SIZE'] == 4) selected @endif>XL</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Width</label>
                            <input type="number" name="width" class="form-control form-control-solid" id="width" placeholder="Masukan Width Size" @if ($mode == "Ubah" && isset($stock)) value="{{ $stock['WIDTH'] }}" @endif>
                        </div>

                        <div class="form-group">
                            <label>Length</label>
                            <input type="number" name="length" class="form-control form-control-solid" id="length" placeholder="Masukan Length Size" @if ($mode == "Ubah" && isset($stock)) value="{{ $stock['LENGTH'] }}" @endif>
                        </div>

                        <div class="form-group">
                            <label>Quantity</label>
                            <input type="number" name="total" class="form-control form-control-solid" id="total" placeholder="Masukan Quantity Stock" @if ($mode == "Ubah" && isset($stock)) value="{{ $stock['TOTAL'] }}" @endif>
                        </div>


                        <div class="form-group">
                            <label>Status</label>
                            <select name="status" class="form-control form-control-solid" id="status">
                                <option value="">-- Pilih Status --</option>
                                <option value="0" @if ($mode == "Ubah" && isset($stock) && $stock['STATUS'] == 0) selected @endif>Unpublish</option>
                                <option value="1" @if ($mode == "Ubah" && isset($stock) && $stock['STATUS'] == 1) selected @endif>Publish</option>
                            </select>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <a class="btn btn-secondary mr-2" href="{{ route('stock.index') }}">Kembali</a>
                            <button type="reset" class="btn btn-danger mr-2">Reset</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection