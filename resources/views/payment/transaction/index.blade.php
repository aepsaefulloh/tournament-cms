@extends('layouts.v1.app')

@section('title', 'Transaction')

@section('transaction', 'menu-item-active')

@section('content')
    {{-- Sub Header Content --}}
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Payment Transaction</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">Informasi Data</span>
            </div>

            <div class="d-flex align-items-center">
                <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">
                    {{ \Carbon\Carbon::now()->format('d m Y') }} <span id="jam"></span> : <span id="menit"></span> : <span
                        id="detik"></span>
                </a>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        {{-- Content --}}
        <div class="container-fluid">
            @if ($message = Session::get('message'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            <div class="row">
                <div class="col-12">
                    <div class="card card-custom gutter-b">
                        <div class="card-header border-0 py-5">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label font-weight-bolder text-dark">Transaction</span>
                            </h3>
                        </div>
                        <div class="card-body p-5">
                            <div class="table-responsive">
                                <table class="table table-head-custom table-vertical-center" id="tb_transaction">
                                    <thead>
                                        <tr class="text-left">
                                            <th>No</th>
                                            <th>Order No</th>
                                            <th>Fullname</th>
                                            <th>Date</th>
                                            <th>Items</th>
                                            <th>Total</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link href="{{ asset('metronic/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@push('scripts')
    {{-- JS Datatables --}}
    <script src="{{ asset('metronic/plugins/custom/datatables/datatables.bundle.js') }}"></script>
    
    <script>
        $(document).ready(function() {
            // Datatables Banner
            $('#tb_transaction').DataTable( {
                "processing": true,
                "ajax": "{{ route('transaction.datatables') }}",
                "columnDefs": [
                    {   
                        "className": "dt-center", 
                        "targets": "_all"
                    }, 
                    { 
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    }
                ],
                "responsive": true,
                "columns": [
                    { "data": "ID",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }    
                    },
                    { "data": "TRID" },
                    { "data": "FULLNAME" },
                    { "data": "TRDATE" },
                    { "data": null,
                      render: function(data) {
                        var result = [];
                        if (data.ITEMS.length > 0) {
                            data.ITEMS.forEach(item => {
                                result += `<span>`+item.CODE +' - Size : '+ item.SIZE + '- Qty : ' + item.QTY+`</span><br>`;
                            });

                            return result;
                        } else {
                            return '-';
                        } 
                      }
                    },
                    { "data": null,
                      render: function(data) {
                        return 'Rp. '+data.TOTAL.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
                      } 
                    },
                    { "data": null,
                      render: function(data) {
                        if (data.STATUS == 0) {
                            return `<a href="{{ url('transaction') }}/`+ data.ID +`" class="label label-lg label-light-success label-inline">Waiting Payment</a>`
                        } else if (data.STATUS == 1) {
                            return `<a href="{{ url('transaction') }}/`+ data.ID +`" class="label label-lg label-light-success label-inline">Payment Complete</a>`
                        } else if (data.STATUS == 2) {
                            return `<a href="{{ url('transaction') }}/`+ data.ID +`" class="label label-lg label-light-success label-inline">Process</a>`
                        } else if (data.STATUS == 3) {
                            return `<a href="{{ url('transaction') }}/`+ data.ID +`" class="label label-lg label-light-success label-inline">Shipping</a>`
                        } else if (data.STATUS == 4) {
                            return `<a href="{{ url('transaction') }}/`+ data.ID +`" class="label label-lg label-light-success label-inline">Complete</a>`
                        }
                      } 
                    }
                ]
            });
        });
    </script>
@endpush
