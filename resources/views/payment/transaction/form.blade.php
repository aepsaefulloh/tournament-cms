@extends('layouts.v1.app')

@section('title', 'Form Transaction')

@section('transaction', 'menu-item-active')

@section('content')
    {{-- Sub Header Content --}}
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Payment Transaction</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">Edit Transaction</span>
            </div>
            
            <div class="d-flex align-items-center">
                <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">
                    {{ \Carbon\Carbon::now()->format('d m Y') }} <span id="jam"></span> : <span id="menit"></span> : <span id="detik"></span>
                </a>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        {{-- Content --}}
        <div class="container">
            @if ($message = Session::get('message'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Form Edit Transaction</h3>
                </div>
                <!--begin::Form-->
                <form class="form" action="{{ route('transaction.store') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $id }}">
                    <div class="card-body">
                        <div class="form-group">
                            <label>Fullname</label>
                            <input type="text" name="fullname" class="form-control form-control-solid" id="fullname" value="{{ $transaction['FULLNAME'] }}">
                        </div>

                        <div class="form-group">
                            <label>Address</label>
                            <textarea name="address" class="form-control form-control-solid" id="address">{{ $transaction['ADDRESS'] }}</textarea>
                        </div>
                    
                        <div class="form-group">
                            <label>Phone</label>
                            <input type="number" name="phone" class="form-control form-control-solid" id="phone" value="{{ $transaction['PHONE'] }}">
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control form-control-solid" id="email" value="{{ $transaction['EMAIL'] }}">
                        </div>

                        <div class="form-group">
                            <label>Province</label>
                            <input type="text" name="province" class="form-control form-control-solid" id="province" value="{{ $transaction['PROV'] }}" readonly>
                        </div>

                        <div class="form-group">
                            <label>City</label>
                            <input type="text" name="city" class="form-control form-control-solid" id="city" value="{{ $transaction['KAB'] }}" readonly>
                        </div>

                        <div class="form-group">
                            <label>Kode Pos</label>
                            <input type="text" name="kodepos" class="form-control form-control-solid" id="kodepos" value="{{ $transaction['KODEPOS'] }}">
                        </div>

                        <div class="form-group">
                            <label>Courier</label>
                            <input type="text" name="courier" class="form-control form-control-solid" id="courier" value="{{ $transaction['SHIPMENT'] }}">
                        </div>

                        <div class="form-group">
                            <label>Shipping Cost</label>
                            <input type="text" name="shipping" class="form-control form-control-solid" id="shipping" value="Rp. {{ number_format($transaction['SHIPPING'], 0, ',', '.') }}">
                        </div>

                        <div class="form-group">
                            <label>Items</label>
                            <div class="mt-2">
                                @forelse ($transaction['ITEMS'] as $item)
                                    <span>{{ $item['CODE'].' - Size: '.$item['SIZE'].' - Qty: '.$item['QTY'] }}</span>
                                @empty
                                    <span>Items Kosong!</span>
                                @endforelse
                            </div>
                            <hr>
                            <span>Total: Rp. {{ number_format($transaction['TOTAL'], 0, ',', '.') }}</span>
                        </div>

                        <div class="form-group">
                            <label>Status</label>
                            <select name="status" class="form-control form-control-solid" id="status">
                                <option value="0" @if ($transaction['STATUS'] == 0) selected @endif>Waiting Payment</option>
                                <option value="1" @if ($transaction['STATUS'] == 1) selected @endif>Payment Success</option>
                                <option value="2" @if ($transaction['STATUS'] == 2) selected @endif>Procces</option>
                                <option value="3" @if ($transaction['STATUS'] == 3) selected @endif>Shipping</option>
                                <option value="4" @if ($transaction['STATUS'] == 4) selected @endif>Complete</option>
                            </select>
                        </div>
                    </div>

                    <div class="card-footer">
                        <div class="text-right">
                            <a class="btn btn-secondary mr-2" href="{{ route('transaction.index') }}">Kembali</a>
                            <button type="reset" class="btn btn-danger mr-2">Reset</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        // Format RP Price
        var rupiah = document.getElementById('shipping');
        rupiah.addEventListener('keyup', function(e){
            rupiah.value = formatRupiah(this.value, 'Rp. ');
        });

        /* Fungsi formatRupiah */
        function formatRupiah(angka, prefix){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split   		= number_string.split(','),
            sisa     		= split[0].length % 3,
            rupiah     		= split[0].substr(0, sisa),
            ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

            if(ribuan){
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }
    </script>
@endpush