<div class="aside aside-left aside-fixed d-flex flex-column flex-row-auto" id="kt_aside">
    <div class="brand flex-column-auto" id="kt_brand">
        <a href="nd-logo">
            <img width="100" alt="Logo" src="{{ asset('metronic/media/logo/logo.png') }}" />
        </a>
        <button class="brand-toggle btn btn-sm px-0" id="kt_aside_toggle">
            <span class="svg-icon svg-icon svg-icon-xl">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                    height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <polygon points="0 0 24 0 24 24 0 24" />
                        <path
                            d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z"
                            fill="#000000" fill-rule="nonzero"
                            transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999)" />
                        <path
                            d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z"
                            fill="#000000" fill-rule="nonzero" opacity="0.3"
                            transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999)" />
                    </g>
                </svg>
            </span>
        </button>
    </div>
    <div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">
        <div id="kt_aside_menu" class="aside-menu my-4" data-menu-vertical="1" data-menu-scroll="1"
            data-menu-dropdown-timeout="500">
            <ul class="menu-nav">

                {{-- List Item Dashboard --}}
                <li class="menu-item @yield('dashboard')" aria-haspopup="true">
                    <a href="{{ route('home') }}" class="menu-link">
                        <i class="flaticon2-analytics-2 mr-5"></i>
                        <span class="menu-text">Dashboard</span>
                    </a>
                </li>

                @if ($_SESSION['SUPERADMIN']['ROLE'] == 1)
                {{-- List Item Administrasi User --}}
                <li class="menu-item @yield('administrasi-user')" aria-haspopup="true">
                    <a href="{{ route('account.index') }}" class="menu-link">
                        <i class="flaticon-users mr-5"></i>
                        <span class="menu-text">Administrasi User</span>
                    </a>
                </li>

                {{-- List Item Member --}}
                <li class="menu-item @yield('member')" aria-haspopup="true">
                    <a href="{{ route('member.index') }}" class="menu-link">
                        <i class="flaticon2-group mr-5"></i>
                        <span class="menu-text">Member</span>
                    </a>
                </li>

                {{-- List Item Banner --}}
                <li class="menu-item @yield('banner')" aria-haspopup="true">
                    <a href="{{ route('content-banner.index') }}" class="menu-link">
                        <i class="flaticon-background mr-5"></i>
                        <span class="menu-text">Banner</span>
                    </a>
                </li>

                {{-- List Item Category --}}
                <li class="menu-item @yield('category')" aria-haspopup="true">
                    <a href="{{ route('category.index') }}" class="menu-link">
                        <i class="flaticon-attachment mr-5"></i>
                        <span class="menu-text">Category</span>
                    </a>
                </li>
                @endif

                {{-- List Item Content --}}
                <li class="menu-item @yield('contents')" aria-haspopup="true">
                    <a href="{{ route('content.index') }}" class="menu-link">
                        <i class="flaticon-edit mr-5"></i>
                        <span class="menu-text">Content</span>
                    </a>
                </li>

                <!-- {{-- List Item Project --}}
                <li class="menu-item @yield('projects')" aria-haspopup="true">
                    <a href="{{ route('project.index') }}" class="menu-link">
                        <i class="flaticon-presentation-1 mr-5"></i>
                        <span class="menu-text">Project</span>
                    </a>
                </li> -->
                {{-- List Item Project --}}
                <li class="menu-item @yield('tournaments')" aria-haspopup="true">
                    <a href="{{ route('tournament.index') }}" class="menu-link">
                        <i class="flaticon-trophy mr-5"></i>
                        <span class="menu-text">Tournament</span>
                    </a>
                </li>

                {{-- List Item Contact --}}
                <li class="menu-item @yield('contents')" aria-haspopup="true">
                    <a href="{{ route('contact.index') }}" class="menu-link">
                        <i class="flaticon-envelope mr-5"></i>
                        <span class="menu-text">Contact</span>
                    </a>
                </li>

                {{-- List Item Product --}}
                <!-- <li class="menu-item @yield('product')" aria-haspopup="true">
                    <a href="{{ route('product.index') }}" class="menu-link">
                        <i class="flaticon-open-box mr-5"></i>
                        <span class="menu-text">Product</span>
                    </a>
                </li> -->

                @if ($_SESSION['SUPERADMIN']['ROLE'] == 1)
                {{-- List Item Transaction --}}
                <!-- <li class="menu-item @yield('transaction')" aria-haspopup="true">
                        <a href="{{ route('transaction.index') }}" class="menu-link">
                            <i class="flaticon-business mr-5"></i>
                            <span class="menu-text">Transaction</span>
                        </a>
                    </li> -->

                {{-- List Item stock --}}
                <!-- <li class="menu-item @yield('stock')" aria-haspopup="true">
                        <a href="{{ route('stock.index') }}" class="menu-link">
                            <i class="flaticon-clipboard mr-5"></i>
                            <span class="menu-text">Stock</span>
                        </a>
                    </li> -->

                {{-- List Item Token Apps --}}
                <!-- <li class="menu-item @yield('token-apps')" aria-haspopup="true">
                        <a href="{{ route('token-apps.index') }}" class="menu-link">
                            <i class="flaticon-settings mr-5"></i>
                            <span class="menu-text">Token Apps</span>
                        </a>
                    </li> -->

                {{-- List Item Setting --}}
                <li class="menu-item @yield('setting')" aria-haspopup="true">
                    <a href="{{ route('setting.index') }}" class="menu-link">
                        <i class="flaticon-settings mr-5"></i>
                        <span class="menu-text">Setting</span>
                    </a>
                </li>
                @endif
            </ul>
        </div>
    </div>
</div>