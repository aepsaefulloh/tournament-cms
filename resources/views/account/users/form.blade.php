@extends('layouts.v1.app')

@section('title', 'Member')

@section('member', 'menu-item-active')

@section('content')
    {{-- Sub Header Content --}}
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Member Akun</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">{{ $mode }} Member Data</span>
            </div>
            
            <div class="d-flex align-items-center">
                <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">
                    {{ \Carbon\Carbon::now()->format('d m Y') }} <span id="jam"></span> : <span id="menit"></span> : <span id="detik"></span>
                </a>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        {{-- Content --}}
        <div class="container-fluid">
            @if ($message = Session::get('message'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Form @if ($mode == "Tambah") Tambah @elseif ($mode == 'Ubah') Edit @endif Member</h3>
                </div>
                <!--begin::Form-->
                <form class="form" action="{{ route('member.store') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="mode" value="{{ $mode }}">
                    @if ($mode == 'Ubah') 
                        <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <div class="card-body">
                        <div class="form-group">
                            <label>Email*</label>
                            <input type="email" name="email" class="form-control form-control-solid" id="email" placeholder="Masukan Email Account*" @if ($mode == "Ubah" && isset($user)) value="{{ $user['EMAIL'] }}" @endif>
                            <span class="form-text text-muted">Atribut Harus diisi!</span>
                        </div>

                        <div class="form-group">
                            <label>Username*</label>
                            <input type="text" name="username" class="form-control form-control-solid" id="username" placeholder="Masukan Username Account*" @if ($mode == "Ubah" && isset($user)) value="{{ $user['USERNAME'] }}" @endif>
                            <span class="form-text text-muted">Atribut Harus diisi!</span>
                        </div>

                        <div class="form-group">
                            <label>Nama Lengkap*</label>
                            <input type="text" name="fullname" class="form-control form-control-solid" id="fullname" placeholder="Masukan Nama Lengkap Account*" @if ($mode == "Ubah" && isset($user)) value="{{ $user['FULLNAME'] }}" @endif>
                            <span class="form-text text-muted">Atribut Harus diisi!</span>
                        </div>

                        <div class="form-group">
                            <label>Password*</label>
                            @if ($mode == 'Ubah')
                                <input type="hidden" name="password_old" value="{{ $user['PASSWD'] }}">
                            @endif
                            <input type="password" name="password" class="form-control form-control-solid" id="password" @if ($mode == 'Tambah') placeholder="Masukan Password Account*" @endif>
                            <span class="form-text text-muted">Atribut Harus diisi!</span>
                            @if ($mode == 'Ubah')
                                <span class="form-text text-muted">Jika Ingin Mengubah Password isi.</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>No. Telepon</label>
                            <input type="number" name="phone" class="form-control form-control-solid" id="phone" placeholder="Masukan No. Telepon" @if ($mode == "Ubah" && isset($user)) value="{{ $user['PHONE'] }}" @endif>
                        </div>

                        <div class="form-group">
                            <label>Role*</label>
                            <select name="role" class="form-control form-control-solid" id="role">
                                <option value="">-- Pilih Role --</option>
                                @foreach ($roles as $role)
                                    <option value="{{ $role['ID'] }}" @if ($mode == "Ubah" && isset($user) && $user['ID_GROUP'] == $role['ID']) selected @endif>{{ $role['GROUP_NAME'] }}</option>                                    
                                @endforeach
                            </select>
                            <span class="form-text text-muted">Atribut Harus diisi!</span>
                        </div>

                        <div class="form-group">
                            <label>Status*</label>
                            <select name="status" class="form-control form-control-solid" id="status">
                                <option value="">-- Pilih Status --</option>
                                <option value="0" @if ($mode == "Ubah" && isset($user) && $user['STATUS'] == 0) selected @endif>Unpublish</option>
                                <option value="1" @if ($mode == "Ubah" && isset($user) && $user['STATUS'] == 1) selected @endif>Publish</option>
                                @if ($mode == 'Ubah')
                                    <option value="99" @if ($mode == "Ubah" && isset($user) && $user['STATUS'] == 99) selected @endif>Delete</option>                                    
                                @endif
                            </select>
                            <span class="form-text text-muted">Atribut Harus diisi!</span>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <a class="btn btn-secondary mr-2" href="{{ route('member.index') }}">Kembali</a>
                            <button type="reset" class="btn btn-danger mr-2">Reset</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection