@extends('layouts.v1.app')

@section('title', 'Content Banner')

@section('banner', 'menu-item-active')

@section('content')
    {{-- Sub Header Content --}}
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Content Banner</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">{{ $mode }} Banner</span>
            </div>
            
            <div class="d-flex align-items-center">
                <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">
                    {{ \Carbon\Carbon::now()->format('d m Y') }} <span id="jam"></span> : <span id="menit"></span> : <span id="detik"></span>
                </a>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        {{-- Content --}}
        <div class="container-fluid">
            @if ($message = Session::get('message'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Form @if ($mode == "Tambah") Tambah @elseif ($mode == 'Ubah') Edit @endif Content Banner</h3>
                </div>
                <!--begin::Form-->
                <form class="form" action="{{ route('content-banner.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="mode" value="{{ $mode }}">
                    @if ($mode == 'Ubah') 
                        <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <div class="card-body">
                        <div class="form-group">
                            <label>Judul*</label>
                            <input type="text" name="title" class="form-control form-control-solid" id="title" placeholder="Masukan Judul*" @if ($mode == "Ubah" && isset($banner)) value="{{ $banner['TITLE'] }}" @endif>
                        </div>

                        <div class="form-group">
                            <label>URL</label>
                            <input type="text" name="url" class="form-control form-control-solid" id="url" placeholder="Masukan Alamat URL" @if ($mode == "Ubah" && isset($banner)) value="{{ $banner['URL'] }}" @endif>
                        </div>

                        <div class="form-group">
                            <label>Posisi*</label>
                            <select name="position" class="form-control form-control-solid" id="position">
                                <option value="">-- Pilih Posisi --</option>
                                <option value="ADS" @if ($mode == "Ubah" && isset($banner) && $banner['POS'] == "ADS") selected @endif>Ads Desktop (160x600)</option>
                                <option value="ADS-MOBILE" @if ($mode == "Ubah" && isset($banner) && $banner['POS'] == "ADS-MOBILE") selected @endif>Ads-Mobile (2022x250)</option>
                                <option value="SLIDER" @if ($mode == "Ubah" && isset($banner) && $banner['POS'] == "SLIDER") selected @endif>Slider</option>
                                <option value="ABOUT-BANNER" @if ($mode == "Ubah" && isset($banner) && $banner['POS'] == "ABOUT-BANNER") selected @endif>About Image</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Urutan Ke</label>
                            <input type="number" name="ordernum" class="form-control form-control-solid" id="ordernum" placeholder="Masukan Urutan Banner" @if ($mode == "Ubah" && isset($banner)) value="{{ $banner['ORDERNUM'] }}" @endif>
                        </div>

                        <div class="form-group">
                            <label>Gambar*</label>
                            @if ($mode == "Ubah" && isset($banner))
                                <div class="text-center" id="box_img_preview">
                                    <img src="{{ $banner['FILENAME'] }}" class="w-50 mb-2"><br>
                                    <button type="button" class="btn btn-sm btn-primary" id="btn_update_image">Perbaharui Gambar</button>
                                </div>

                                <div id="box_image" style="display: none;">
                                    <input type="file" name="gambar" class="form-control form-control-solid" id="gambar">
                                    <center>
                                        <img id="preview-image-before-upload" src="" alt="preview image" class="w-50 mt-5">
                                    </center>
                                </div>
                            @else   
                                <input type="file" name="gambar" class="form-control form-control-solid" id="gambar">
                                <center>
                                    <img id="preview-image-before-upload" src="" alt="preview image" class="w-50 mt-5">
                                </center>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Status*</label>
                            <select name="status" class="form-control form-control-solid" id="status">
                                <option value="">-- Pilih Status --</option>
                                <option value="0" @if ($mode == "Ubah" && isset($banner) && $banner['STATUS'] == 0) selected @endif>Unpublish</option>
                                <option value="1" @if ($mode == "Ubah" && isset($banner) && $banner['STATUS'] == 1) selected @endif>Publish</option>
                            </select>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <a class="btn btn-secondary mr-2" href="{{ route('content-banner.index') }}">Kembali</a>
                            <button type="reset" class="btn btn-danger mr-2">Reset</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function (e) { 
            // Onclick Update Image
            $('#btn_update_image').click(function() {
                $('#box_img_preview').hide();
                $('#box_image').show();
            })

            // Image Preview
            $('#preview-image-before-upload').hide();
            $('#gambar').change(function(){
                let reader = new FileReader();
                reader.onload = (e) => { 
                    $('#preview-image-before-upload').show().attr('src', e.target.result); 
                }
                reader.readAsDataURL(this.files[0]); 
            });
        });
    </script>
@endpush