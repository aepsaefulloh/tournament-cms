@extends('layouts.v1.app')

@section('title', 'Form Token Apps')

@section('token-apps', 'menu-item-active')

@section('content')
    {{-- Sub Header Content --}}
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Token Apps</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">{{ $mode }} Token Apps</span>
            </div>
            
            <div class="d-flex align-items-center">
                <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">
                    {{ \Carbon\Carbon::now()->format('d m Y') }} <span id="jam"></span> : <span id="menit"></span> : <span id="detik"></span>
                </a>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        {{-- Content --}}
        <div class="container-fluid">
            @if ($message = Session::get('message'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Form @if ($mode == "Tambah") Tambah @elseif ($mode == 'Ubah') Edit @endif Token Apps</h3>
                </div>
                <!--begin::Form-->
                <form class="form" action="{{ route('token-apps.store') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="mode" value="{{ $mode }}">
                    @if ($mode == 'Ubah') 
                        <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <div class="card-body">
                        <div class="form-group">
                            <label>Name Apps*</label>
                            <input type="text" name="name" class="form-control form-control-solid" id="name" placeholder="Masukan Name Apps*" @if ($mode == "Ubah" && isset($token)) value="{{ $token['NAME'] }}" @endif>
                        </div>

                        <div class="form-group">
                            <label>Token Key*</label>
                            <input type="text" name="token" class="form-control form-control-solid" readonly id="token" @if ($mode == "Ubah" && isset($token)) value="{{ $token['TOKEN'] }}" @else value="{{ base64_encode(\Str::random(50)) }}" @endif>
                        </div>

                        <div class="form-group">
                            <label>Status*</label>
                            <select name="status" class="form-control form-control-solid" id="status">
                                <option value="">-- Pilih Status --</option>
                                <option value="0" @if ($mode == "Ubah" && isset($token) && $token['STATUS'] == 0) selected @endif>Nonaktif</option>
                                <option value="1" @if ($mode == "Ubah" && isset($token) && $token['STATUS'] == 1) selected @endif>Aktif</option>
                            </select>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <a class="btn btn-secondary mr-2" href="{{ route('token-apps.index') }}">Kembali</a>
                            <button type="reset" class="btn btn-danger mr-2">Reset</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection