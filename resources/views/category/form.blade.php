@extends('layouts.v1.app')

@section('title', 'Form Category')

@section('category', 'menu-item-active')

@section('content')
    {{-- Sub Header Content --}}
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Category</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">{{ $mode }} Category</span>
            </div>
            
            <div class="d-flex align-items-center">
                <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">
                    {{ \Carbon\Carbon::now()->format('d m Y') }} <span id="jam"></span> : <span id="menit"></span> : <span id="detik"></span>
                </a>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        {{-- Content --}}
        <div class="container-fluid">
            @if ($message = Session::get('message'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Form @if ($mode == "Tambah") Tambah @elseif ($mode == 'Ubah') Edit @endif Category</h3>
                </div>
                <!--begin::Form-->
                <form class="form" action="{{ route('category.store') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="mode" value="{{ $mode }}">
                    @if ($mode == 'Ubah') 
                        <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <div class="card-body">
                        <div class="form-group">
                            <label>Category*</label>
                            <input type="text" name="category" class="form-control form-control-solid" id="category" placeholder="Masukan Category*" @if ($mode == "Ubah" && isset($category)) value="{{ $category['CATEGORY'] }}" @endif>
                        </div>

                        <div class="form-group">
                            <label>Seo*</label>
                            <input type="text" name="seo" class="form-control form-control-solid" id="seo" placeholder="Masukan SEO" @if ($mode == "Ubah" && isset($category)) value="{{ $category['SEO'] }}" @endif>
                        </div>

                        <div class="form-group">
                            <label>Tipe*</label>
                            <select name="tipe" class="form-control form-control-solid" id="tipe">
                                <option value="">-- Pilih Tipe --</option>
                                <option value="content" @if ($mode == "Ubah" && isset($category) && $category['TIPE'] == "content") selected @endif>Content</option>
                                <option value="project" @if ($mode == "Ubah" && isset($category) && $category['TIPE'] == "project") selected @endif>Project</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Status*</label>
                            <select name="status" class="form-control form-control-solid" id="status">
                                <option value="">-- Pilih Status --</option>
                                <option value="0" @if ($mode == "Ubah" && isset($category) && $category['STATUS'] == 0) selected @endif>Unpublish</option>
                                <option value="1" @if ($mode == "Ubah" && isset($category) && $category['STATUS'] == 1) selected @endif>Publish</option>
                            </select>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <a class="btn btn-secondary mr-2" href="{{ route('category.index') }}">Kembali</a>
                            <button type="reset" class="btn btn-danger mr-2">Reset</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection