@extends('layouts.v1.app')

@section('title', 'Form Tournament')

@section('tournaments', 'menu-item-active')

@section('content')
    {{-- Sub Header Tournament --}}
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Tournament</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">{{ $mode }} Tournament</span>
            </div>
            
            <div class="d-flex align-items-center">
                <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">
                    {{ \Carbon\Carbon::now()->format('d m Y') }} <span id="jam"></span> : <span id="menit"></span> : <span id="detik"></span>
                </a>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        {{-- Tournament --}}
        <div class="container-fluid">
            @if ($message = Session::get('message'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Form @if ($mode == "Tambah") Tambah @elseif ($mode == 'Ubah') Edit @endif Tournament</h3>
                </div>
                <!--begin::Form-->
                <form class="form" action="{{ route('tournament.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="mode" value="{{ $mode }}">
                    @if ($mode == 'Ubah') 
                        <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <div class="card-body">
                        <div class="form-group">
                            <label>Judul</label>
                            <input type="text" name="title" class="form-control form-control-solid" id="title" placeholder="Masukan Judul*" @if ($mode == "Ubah" && isset($project)) value="{{ $project['TITLE'] }}" @endif>
                        </div>

                        <div class="form-group">
                            <label>Summary</label>
                            <input type="text" name="summary" class="form-control form-control-solid" id="summary" placeholder="Masukan Summary" @if ($mode == "Ubah" && isset($project)) value="{{ $project['SUMMARY'] }}" @endif>
                            <span class="form-text text-muted">Meta Description SEO</span>
                        </div>

                        <div class="form-group">
                            <label>Keyword</label>
                            <input type="text" name="keyword" class="form-control form-control-solid" id="keyword" placeholder="Masukan Keyword" @if ($mode == "Ubah" && isset($project)) value="{{ $project['KEYWORD'] }}" @endif>
                            <span class="form-text text-muted">Untuk Membantu Pada SEO</span>
                        </div>

                        <div class="form-group">
                            <label>Subtitle</label>
                            <input type="text" name="subtitle" class="form-control form-control-solid" id="subtitle" placeholder="Masukan Subtitle*" @if ($mode == "Ubah" && isset($project)) value="{{ $project['SUBTITLE'] }}" @endif>
                        </div>

                        <div class="form-group">
                            <label>Creator</label>
                            <input type="text" name="creator" class="form-control form-control-solid" id="creator" placeholder="Masukan Creator*" @if ($mode == "Ubah" && isset($project)) value="{{ $project['CREATE_BY'] }}" @endif>
                        </div>

                        <div class="form-group">
                            <label>Category</label>
                            <select name="category" class="form-control form-control-solid" id="category">
                                <option value="">-- Pilih Category --</option>
                                @foreach ($category as $item)
                                    <option value="{{ $item['ID'] }}" @if ($mode == "Ubah" && isset($project) && $project['CATEGORY'] == $item['ID']) selected @endif>{{ $item['CATEGORY'] }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Content</label>
                            <textarea name="content" class="form-control form-control-solid" id="content" placeholder="Masukan Project">@if ($mode == "Ubah" && isset($project)) {{ $project['CONTENT'] }} @endif</textarea>
                        </div>

                        <div class="form-group">
                            <label>Gambar</label>
                            @if ($mode == "Ubah" && isset($project))
                                <div class="text-center" id="box_img_preview">
                                    <img src="{{ $project['IMAGE'] }}" class="w-50 mb-2"><br>
                                    <button type="button" class="btn btn-sm btn-primary" id="btn_update_image">Perbaharui Gambar</button>
                                </div>

                                <div id="box_image" style="display: none;">
                                    <input type="file" name="gambar" class="form-control form-control-solid" id="gambar">
                                    <span class="form-text text-muted">Ukuran Gambar 1240x656 & kurang dari 300kb</span>
                                    <center>
                                        <img id="preview-image-before-upload" src="" alt="preview image" class="w-50 mt-5">
                                    </center>
                                </div>
                            @else   
                                <input type="file" name="gambar" class="form-control form-control-solid" id="gambar">
                                <span class="form-text text-muted">Ukuran Gambar 1240x656 & kurang dari 300kb</span>
                                <center>
                                    <img id="preview-image-before-upload" src="" alt="preview image" class="w-50 mt-5">
                                </center>
                                
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Status</label>
                            <select name="status" class="form-control form-control-solid" id="status">
                                <option value="">-- Pilih Status --</option>
                                <option value="0" @if ($mode == "Ubah" && isset($project) && $project['STATUS'] == 0) selected @endif>Unpublish</option>
                                <option value="1" @if ($mode == "Ubah" && isset($project) && $project['STATUS'] == 1) selected @endif>Publish</option>
                            </select>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <a class="btn btn-secondary mr-2" href="{{ route('project.index') }}">Kembali</a>
                            <button type="reset" class="btn btn-danger mr-2">Reset</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    {{-- CKeditor 4 --}}
    <script src="//cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
    <script>
        $(document).ready(function (e) {
            // CKEDITOR
            CKEDITOR.replace('content', {
                filebrowserUploadUrl: "{{ route('project.upload-image', ['_token' => csrf_token() ]) }}",
                filebrowserUploadMethod: 'form'
            });

            // Onclick Update Image
            $('#btn_update_image').click(function() {
                $('#box_img_preview').hide();
                $('#box_image').show();
            })

            // Image Preview
            $('#preview-image-before-upload').hide();
            $('#gambar').change(function(){
                let reader = new FileReader();
                reader.onload = (e) => { 
                    $('#preview-image-before-upload').show().attr('src', e.target.result); 
                }
                reader.readAsDataURL(this.files[0]); 
            });
        });
    </script>
@endpush