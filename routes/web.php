<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Account\AccountController;
use App\Http\Controllers\Member\MemberController;
use App\Http\Controllers\Account\RoleController;
use App\Http\Controllers\Content\BannerController;
use App\Http\Controllers\Category\CategoryController;
use App\Http\Controllers\Content\ContentController;
use App\Http\Controllers\Contact\ContactController;
use App\Http\Controllers\Project\ProjectController;
use App\Http\Controllers\Product\ProductController;
use App\Http\Controllers\Product\StockController;
use App\Http\Controllers\Payment\TransactionController;
use App\Http\Controllers\Setting\SettingController;
use App\Http\Controllers\Tournament\TournamentController;
use App\Http\Controllers\TokenController;



Route::get('/', function () {
    return redirect()->route('home');
});

// Auth
Route::get('/login', [AuthController::class, 'login'])->name('login');
Route::post('/login-post', [AuthController::class, 'login_post'])->name('login-post');
Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
Route::get('/users-logs', [AuthController::class, 'logs'])->name('logs');

Route::get('/home', [HomeController::class, 'index'])->name('home');

// Account Module
Route::get('/account', [AccountController::class, 'index'])->name('account.index');
Route::get('/account-table', [AccountController::class, 'datatables'])->name('account.datatables');
Route::get('/account-add', [AccountController::class, 'add'])->name('account.add');
Route::get('/account/{id}', [AccountController::class, 'edit'])->name('account.edit');
Route::post('/account-store', [AccountController::class, 'store'])->name('account.store');

// Member Module
Route::get('/member', [MemberController::class, 'index'])->name('member.index');
Route::get('/member-table', [MemberController::class, 'datatables'])->name('member.datatables');
Route::get('/member-add', [MemberController::class, 'add'])->name('member.add');
Route::get('/member/{id}', [MemberController::class, 'edit'])->name('member.edit');
Route::post('/member-store', [MemberController::class, 'store'])->name('member.store');

// Account Role Module
Route::get('/account-role-table', [RoleController::class, 'datatables'])->name('account-role.datatables');
Route::get('/account-role-add', [RoleController::class, 'add'])->name('account-role.add');
Route::get('/account-role-edit/{id}', [RoleController::class, 'edit'])->name('account-role.edit');
Route::post('/account-role-store', [RoleController::class, 'store'])->name('account-role.store');

// Content Banner Module
Route::get('/content-banner', [BannerController::class, 'index'])->name('content-banner.index');
Route::get('/content-banner-table', [BannerController::class, 'datatables'])->name('content-banner.datatables');
Route::get('/content-banner-add', [BannerController::class, 'add'])->name('content-banner.add');
Route::get('/content-banner/{id}', [BannerController::class, 'edit'])->name('content-banner.edit');
Route::post('/content-banner-store', [BannerController::class, 'store'])->name('content-banner.store');

// Category Module
Route::get('/category', [CategoryController::class, 'index'])->name('category.index');
Route::get('/category-table', [CategoryController::class, 'datatables'])->name('category.datatables');
Route::get('/category-add', [CategoryController::class, 'add'])->name('category.add');
Route::get('/category/{id}', [CategoryController::class, 'edit'])->name('category.edit');
Route::post('/category-store', [CategoryController::class, 'store'])->name('category.store');

// Content Module
Route::get('/content', [ContentController::class, 'index'])->name('content.index');
Route::get('/content-table', [ContentController::class, 'datatables'])->name('content.datatables');
Route::get('/content-add', [ContentController::class, 'add'])->name('content.add');
Route::get('/content/{id}', [ContentController::class, 'edit'])->name('content.edit');
Route::post('/content-store', [ContentController::class, 'store'])->name('content.store');
Route::post('/content-store-image', [ContentController::class, 'upload_image'])->name('content.upload-image');

// Project Module
Route::get('/project', [ProjectController::class, 'index'])->name('project.index');
Route::get('/project-table', [ProjectController::class, 'datatables'])->name('project.datatables');
Route::get('/project-add', [ProjectController::class, 'add'])->name('project.add');
Route::get('/project/{id}', [ProjectController::class, 'edit'])->name('project.edit');
Route::post('/project-store', [ProjectController::class, 'store'])->name('project.store');
Route::post('/project-store-image', [ProjectController::class, 'upload_image'])->name('project.upload-image');

// Tournament Module
Route::get('tournament', [TournamentController::class, 'index'])->name('tournament.index');
Route::get('tournament-table', [TournamentController::class, 'datatables'])->name('tournament.datatables');
Route::get('tournament-add', [TournamentController::class, 'add'])->name('tournament.add');
Route::get('tournament/{id}', [TournamentController::class, 'edit'])->name('tournament.edit');
Route::post('tournament-store', [TournamentController::class, 'store'])->name('tournament.store');
Route::post('tournament-store-image', [TournamentController::class, 'upload_image'])->name('tournament.upload-image');

// Product Module
Route::get('/product', [ProductController::class, 'index'])->name('product.index');
Route::get('/product-table', [ProductController::class, 'datatables'])->name('product.datatables');
Route::get('/product-add', [ProductController::class, 'add'])->name('product.add');
Route::get('/product/{id}', [ProductController::class, 'edit'])->name('product.edit');
Route::post('/product-store', [ProductController::class, 'store'])->name('product.store');
Route::get('/product-remove-image', [ProductController::class, 'remove_image'])->name('product.remove-image');
Route::get('/product-status', [ProductController::class, 'status_product'])->name('product.status');

// Product Stock Module
Route::get('/product-stock', [StockController::class, 'index'])->name('stock.index');
Route::get('/product-stock-table', [StockController::class, 'datatables'])->name('stock.datatables');
Route::get('/product-stock-balance', [StockController::class, 'stock_balance'])->name('stock-balance');
Route::get('/product-stock-add', [StockController::class, 'add'])->name('stock.add');
Route::get('/product-stock/{id}', [StockController::class, 'edit'])->name('stock.edit');
Route::post('/product-stock-store', [StockController::class, 'store'])->name('stock.store');

// Payment Transaction Module
Route::get('/transaction', [TransactionController::class, 'index'])->name('transaction.index');
Route::get('/transaction-table', [TransactionController::class, 'datatables'])->name('transaction.datatables');
Route::get('/transaction/{id}', [TransactionController::class, 'edit'])->name('transaction.edit');
Route::post('/transaction-store', [TransactionController::class, 'store'])->name('transaction.store');

// Contact Modul
Route::get('/contact', [ContactController::class, 'index'])->name('contact.index');
// Route::get('/project-table', [projectController::class, 'datatables'])->name('project.datatables');
// Route::get('/project-add', [projectController::class, 'add'])->name('project.add');
// Route::get('/project/{id}', [projectController::class, 'edit'])->name('project.edit');
// Route::post('/project-store', [projectController::class, 'store'])->name('project.store');
// Route::post('/project-store-image', [projectController::class, 'upload_image'])->name('project.upload-image');

// Setting Apps Module
Route::get('/setting-apps', [SettingController::class, 'index'])->name('setting.index');
Route::post('/setting-apps-store', [SettingController::class, 'store'])->name('setting.store');

Route::get('/token-apps', [TokenController::class, 'index'])->name('token-apps.index');
Route::get('/token-apps-table', [TokenController::class, 'datatables'])->name('token-apps.datatables');
Route::get('/token-apps-add', [TokenController::class, 'add'])->name('token-apps.add');
Route::get('/token-apps/{id}', [TokenController::class, 'edit'])->name('token-apps.edit');
Route::post('/token-apps-store', [TokenController::class, 'store'])->name('token-apps.store');